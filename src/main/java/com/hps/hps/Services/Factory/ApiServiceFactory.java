package com.hps.hps.Services.Factory;

import com.hps.hps.Services.ApiService;
import com.hps.hps.Services.recommendationServices.*;
import com.hps.hps.Utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApiServiceFactory {

    @Autowired
    RelatedService relatedService ;


    @Autowired
    TrendingService trendingService ;


    @Autowired
    AutoMlservice autoMlservice ;


    @Autowired
    Collaborative collaborative ;

    @Autowired
    BecauseYouWatched becauseYouWatched ;

    @Autowired
    MomentService momentService ;

    @Autowired
    FileService file ;


    public ApiService getService(String serviceName){

        switch (serviceName) {
            case Constants.RELATED :
                return relatedService;
            case Constants.TRENDING:
                return trendingService;
            case Constants.AUTOML:
                return autoMlservice;
            case Constants.COLLABORATIVE :
                return collaborative;
            case Constants.BECAUSEYOUWATCHED :
                return becauseYouWatched;
            case Constants.MOMENTS :
                return momentService;
            case Constants.FILE :
                return file;
            }
            return null;
        }



}
