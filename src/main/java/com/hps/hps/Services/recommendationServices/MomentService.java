package com.hps.hps.Services.recommendationServices;


import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Services.ApiService;
import com.hps.hps.Utils.HomePageUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.concurrent.CompletableFuture;

@Service
public  class MomentService implements ApiService {

    @Autowired
    private RestTemplate restTemplate;
    @Value("${recommend.momentsCollaborativeFilterApi}")
    String momentsApi;

    @Autowired
    HomePageUtil homePageUtil ;


    @Async
    public CompletableFuture<RowAnswer> getContent(HomePageRequest homePageRequest , RowAnswer rowAnswer) {
        String momentsApiFormatted = MessageFormat.format(momentsApi, homePageRequest.getAppid(), homePageRequest.getApp_lang(), homePageRequest.getUid(), homePageRequest.getUser_ccode(), homePageRequest.getUser_geo(),homePageUtil.getRecentlyWatchedCid(homePageRequest), "", homePageRequest.getRegionid(), homePageRequest.getUser_ccode() , homePageRequest.getUser_content_preference(), homePageRequest.getUser_segment());
        String body  ="" ;
        if(homePageRequest.getPaginationRequired() && homePageRequest.getRan_sync_no()!=null){
            body =  homePageRequest.toMomentsStringWithPaginationWithNoRanSync();
        }else if(homePageRequest.getPaginationRequired()){
            body = homePageRequest.toMomentsStringWithPagination() ;
        }else {
            body =  homePageRequest.toMoments();
        }
        ResponseEntity<String> response =   restTemplate.exchange(momentsApiFormatted,
                HttpMethod.GET,
                new HttpEntity<>(body , HomePageUtil.getHeadersBYW(homePageRequest)),
                String.class);
        if(response.getStatusCode().is2xxSuccessful() && response.getBody().length()>10) {
            rowAnswer.setServiceResponse(new JSONObject(response.getBody()));
        }

        return  CompletableFuture.completedFuture(rowAnswer);
    }

}
