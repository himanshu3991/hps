package com.hps.hps.Services.recommendationServices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RecommendationResponse;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Services.ApiService;
import com.hps.hps.Utils.HomePageUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class Collaborative implements ApiService {


    @Autowired
    private RestTemplate restTemplate;
    @Value("${recommend.recommendationcollaborativefilterapi}")
    String recommendationCollaborativeFilterApi;

    @Autowired
    HomePageUtil homePageUtil ;

    @Async
    public CompletableFuture<RowAnswer> getContent(HomePageRequest homePageRequest , RowAnswer rowAnswer) {


        String recommendationCollaborativeFilterApiUrl = MessageFormat.format(recommendationCollaborativeFilterApi, homePageRequest.getAppid(), homePageRequest.getApp_lang(), homePageRequest.getUid(), homePageRequest.getUser_ccode(), homePageRequest.getUser_geo(),homePageUtil.getRecentlyWatchedCid(homePageRequest), "", homePageRequest.getRegionid(), homePageRequest.getUser_ccode() , homePageRequest.getUser_content_preference(), homePageRequest.getUser_segment());
        ResponseEntity<String> response =   restTemplate.exchange(recommendationCollaborativeFilterApiUrl,
                HttpMethod.GET,
                new HttpEntity<>(HomePageUtil.getHeadersBYW(homePageRequest)),
                String.class);
        if(response.getStatusCode().is2xxSuccessful() && response.getBody().length()>10) {
            rowAnswer.setServiceResponse(new JSONObject(response.getBody()));
        }

        JSONObject jsonObject = null ;
        try {
            rowAnswer.setSlug("inhouseRecom");
            rowAnswer.setTitle("inhouseRecom");
            rowAnswer.setRecommend("inhouseRecom");
            rowAnswer.setPath("$..response.container[*].item");
            rowAnswer.setRawContent(response.getBody());
            jsonObject =  homePageUtil.modifyContainer(rowAnswer);
           } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if(response.getStatusCode().is2xxSuccessful() && response.getBody().length()>10) {
            rowAnswer.setServiceResponse(jsonObject);
        }
        return  CompletableFuture.completedFuture(rowAnswer);
    }


}