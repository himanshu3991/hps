package com.hps.hps.Services.recommendationServices;

import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Services.ApiService;
import com.hps.hps.Services.URLService;
import com.hps.hps.Utils.HomePageUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class FileService implements ApiService {

    @Autowired
    URLService urlService ;

    @Autowired
    HomePageUtil homePageUtil ;


    @Async
    public CompletableFuture<RowAnswer> getContent(HomePageRequest homePageRequest, RowAnswer rowAnswer)  {
        JSONObject js = urlService.getContent(homePageRequest);
        List<String> list = homePageUtil.getJsonPathdataasList("$.container[?(@.id=='"+rowAnswer.getServiceId()+"')]",String.valueOf(js));
        System.out.println("content from file"+list.toString());
        rowAnswer.setServiceResponse(new JSONArray(String.valueOf(list)).getJSONObject(0));
        return  CompletableFuture.completedFuture(rowAnswer);
    }

}
