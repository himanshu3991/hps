package com.hps.hps.Services.recommendationServices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RecommendationResponse;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Services.ApiService;
import com.hps.hps.Utils.HomePageUtil;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class RelatedService implements ApiService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${recommend.related}")
    String relatedUrl;

    @Autowired
    HomePageUtil homePageUtil ;

    @Async
    public CompletableFuture<RowAnswer> getContent(HomePageRequest homePageRequest, RowAnswer rowAnswer) {
        JSONObject playlistId = getPlayList(homePageRequest);
        String pId = playlistId.optString("playlist_id");
        String pl_title = playlistId.optString("playlist_title");
        String recommendUrl = MessageFormat.format(relatedUrl, pId, homePageRequest.getAppid(), homePageRequest.getVersionName(), homePageRequest.getUser_ccode(), homePageRequest.getUser_ccode(), homePageRequest.getUser_ccode(), homePageRequest.getUser_geo(), homePageRequest.getIid(), homePageRequest.getApp_lang(), homePageRequest.getUid(), homePageRequest.getUserId());
        System.out.println("related Url " + recommendUrl);
        ResponseEntity<String> response = restTemplate.exchange(recommendUrl,
                HttpMethod.GET,
                new HttpEntity<>(HomePageUtil.getHeadersBYW(homePageRequest)),
                String.class);
        System.out.println("response is "+response.getBody());
        JSONObject jsonObject = XML.toJSONObject(response.getBody(), true);
        System.out.println("response after to jso n is "+jsonObject);



        JSONObject jsonObjectResponse = null;
        try {
            rowAnswer.setSlug("related");
            rowAnswer.setTitle(pl_title);
            rowAnswer.setRecommend("related");
            rowAnswer.setPath("$..response.container.item");
            rowAnswer.setRawContent(response.getBody());
            jsonObjectResponse =  homePageUtil.modifyContainer(rowAnswer);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if (response.getStatusCode().is2xxSuccessful() && response.getBody().length() > 10) {
            rowAnswer.setServiceResponse(jsonObjectResponse);
        }
        return CompletableFuture.completedFuture(rowAnswer);
    }

    // todo hardcoded
    private JSONObject getPlayList(HomePageRequest homePageRequest) {
        int percentThreshold = 20 ;
        JSONObject pl = null ;
        String recentlyWatched = homePageRequest.getRecently_watched();
        if (StringUtils.trimToNull(recentlyWatched) != null) {
            try {
                JSONArray recentlyWatchedPlaylists = new JSONArray(recentlyWatched);
                for (int i = 0; i < recentlyWatchedPlaylists.length(); i++) {
                     pl = recentlyWatchedPlaylists.getJSONObject(i);
                    int percent_content_consumed = pl.optInt("percent_content_consumed");
                    System.out.println("percentage Consumerd "+percent_content_consumed);
                    if (percent_content_consumed <= percentThreshold) {
                        pl = null ;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return pl;
    }

}