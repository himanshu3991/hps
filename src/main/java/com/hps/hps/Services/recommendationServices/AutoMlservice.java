package com.hps.hps.Services.recommendationServices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RecommendationResponse;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Services.ApiService;
import com.hps.hps.Utils.HomePageUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static com.hps.hps.Utils.HomePageUtil.defaultMethod;

@Service
public class AutoMlservice implements ApiService {

    @Value("${recommend.recommendationautoml}")
    String recommendationAutoMl;

    @Autowired
    HomePageUtil homePageUtil ;

     @Autowired
        private RestTemplate restTemplate;

    @Async
    public CompletableFuture<RowAnswer> getContent(HomePageRequest homePageRequest , RowAnswer rowAnswer)  {
        String recommendationautoml = MessageFormat.format(recommendationAutoMl, homePageRequest.getAppid(), homePageRequest.getApp_lang(), homePageRequest.getUid(), homePageRequest.getUser_ccode(), homePageRequest.getUser_geo(),
                homePageUtil.getRecentlyWatchedCid(homePageRequest), "", homePageRequest.getRegionid(), homePageRequest.getUser_ccode() , homePageRequest.getUser_content_preference(), homePageRequest.getUser_segment());
        ResponseEntity<String> response =   restTemplate.exchange(recommendationautoml,
                HttpMethod.GET,
                new HttpEntity<>(HomePageUtil.getHeadersBYW(homePageRequest)),
                String.class);
       JSONObject jsonObject = null ;
        try {
            System.out.println("auto ml ");
            rowAnswer.setSlug("automl");
            rowAnswer.setTitle("automl");
            rowAnswer.setRecommend("automl");
            rowAnswer.setPath("$..response.container[*].item");
            rowAnswer.setRawContent(response.getBody());
            jsonObject =  homePageUtil.modifyContainer(rowAnswer);
            System.out.println("auto ml res" + jsonObject.toString());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if(response.getStatusCode().is2xxSuccessful() && response.getBody().length()>10) {
            rowAnswer.setServiceResponse(jsonObject);
        }
        // get the item : $..response.container[:].item[:]
        return  CompletableFuture.completedFuture(rowAnswer);
    }


}