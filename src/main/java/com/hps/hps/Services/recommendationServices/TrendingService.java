package com.hps.hps.Services.recommendationServices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RecommendationResponse;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Services.ApiService;
import com.hps.hps.Utils.HomePageUtil;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class TrendingService implements ApiService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${recommend.trending}")
    String recommendTrendingUrl;

    @Autowired
    HomePageUtil homePageUtil ;

    @Async
    public CompletableFuture<RowAnswer> getContent(HomePageRequest homePageRequest , RowAnswer rowAnswer) {

        String trendingUrl = MessageFormat.format(recommendTrendingUrl, StringUtils.trimToEmpty(homePageRequest.getUser_ccode()).toUpperCase());
        String body = homePageRequest.toTrending();
        System.out.println(body);
        ResponseEntity<String> response =   restTemplate.exchange(trendingUrl,
                HttpMethod.POST,
                new HttpEntity<>(body , HomePageUtil.getHeadersBYW(homePageRequest)),
                String.class);
        JSONObject jsonObject = null ;
        try {
            rowAnswer.setSlug("trending");
            rowAnswer.setTitle("Recommendation");
            rowAnswer.setRecommend("trending");
            rowAnswer.setPath("$..response.container[*].item");
            rowAnswer.setRawContent(response.getBody());
            jsonObject =  homePageUtil.modifyContainer(rowAnswer);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        if(response.getStatusCode().is2xxSuccessful() && response.getBody().length()>10) {
            rowAnswer.setServiceResponse(jsonObject);
        }

        return  CompletableFuture.completedFuture(rowAnswer);
    }




}