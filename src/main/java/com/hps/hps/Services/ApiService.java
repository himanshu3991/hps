package com.hps.hps.Services;

import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RowAnswer;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
 public interface ApiService {
    @Async
      CompletableFuture<RowAnswer> getContent(HomePageRequest homePageRequest, RowAnswer rowAnswer);
}

