package com.hps.hps.Services;

import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Utils.Constants;
import com.hps.hps.Utils.HomePageUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class URLService {

    @Autowired
    private HomePageUtil homePageUtil ;

    @Value("${pageSize}")
    private int pageSize;

    @Autowired
    CacheManager cacheManager;


    @Cacheable(value = "content", key = "#homePageRequest.getUrl()" , condition="#homePageRequest.getUrl()!= null")
    public JSONObject getContent(HomePageRequest homePageRequest) throws JSONException {
        JSONObject jsonObject = null ;
        String js = "{\n" +
                "  \"container\": [\n" +
                "    {\n" +
                "      \"containertype\": \"spotlight\",\n" +
                "      \"original_row_pos\": 0,\n" +
                "      \"item\": [\n" +
                "        {\n" +
                "          \"tcid_16x9\": \"1159364136\",\n" +
                "          \"preferred_thumb\": \"withtitle\",\n" +
                "          \"createtime\": \"1577772369937\",\n" +
                "          \"playlistItemsCount\": \"20\",\n" +
                "          \"language\": \"Korean\",\n" +
                "          \"type\": \"playlist\",\n" +
                "          \"title\": \"My Lawyer Mr Joe\",\n" +
                "          \"contenttype\": \"tvshows\",\n" +
                "          \"tcid_2x3_t\": \"1165570577\",\n" +
                "          \"original_row_pos\": 0,\n" +
                "          \"total\": \"20\",\n" +
                "          \"tcid_2x3\": \"1165570577\",\n" +
                "          \"bgcolor\": \"#581047\",\n" +
                "          \"spotlight_tcid_16x9\": \"1165570576\",\n" +
                "          \"paid\": \"false\",\n" +
                "          \"tver\": \"23\",\n" +
                "          \"id\": \"playlist-25901565\",\n" +
                "          \"updatetime\": \"1577772369937\",\n" +
                "          \"tcid_16x9_t\": \"1165570576\",\n" +
                "          \"tcid_38x13_d\": \"1165576856\",\n" +
                "          \"tcid\": \"1114486688\",\n" +
                "          \"slug\": \"my_lawyer_mr_joe\",\n" +
                "          \"original_col_pos\": 0\n" +
                "        }\n" +
                "      ],\n" +
                "      \"logic\": \"Curated\",\n" +
                "      \"id\": \"0_0_30_1\",\n" +
                "      \"title\": \"Spotlight\",\n" +
                "      \"type\": \"spotlight\",\n" +
                "      \"slug\": \"spotlight\",\n" +
                "      \"contenttype\": \"tvshows\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"containertype\": \"list\",\n" +
                "      \"original_row_pos\": 1,\n" +
                "      \"item\": [\n" +
                "        {\n" +
                "          \"tcid_16x9\": \"1165579054\",\n" +
                "          \"preferred_thumb\": \"withtitle\",\n" +
                "          \"createtime\": \"1577772392952\",\n" +
                "          \"playlistItemsCount\": \"4\",\n" +
                "          \"language\": \"Burmese-unicode\",\n" +
                "          \"type\": \"playlist\",\n" +
                "          \"title\": \"My Fellow Citizens\",\n" +
                "          \"contenttype\": \"tvshows\",\n" +
                "          \"tcid_2x3_t\": \"1165578764\",\n" +
                "          \"original_row_pos\": 0,\n" +
                "          \"total\": \"4\",\n" +
                "          \"bgcolor\": \"#894040\",\n" +
                "          \"paid\": \"false\",\n" +
                "          \"tver\": \"2\",\n" +
                "          \"id\": \"playlist-25929886\",\n" +
                "          \"updatetime\": \"1577772392952\",\n" +
                "          \"tcid\": \"1165578987\",\n" +
                "          \"slug\": \"my_fellow_citizens\",\n" +
                "          \"original_col_pos\": 1\n" +
                "        },\n" +
                "        {\n" +
                "          \"tcid_16x9\": \"1165067610\",\n" +
                "          \"preferred_thumb\": \"withtitle\",\n" +
                "          \"createtime\": \"1538475784551\",\n" +
                "          \"playlistItemsCount\": \"20\",\n" +
                "          \"language\": \"Korean\",\n" +
                "          \"type\": \"playlist\",\n" +
                "          \"title\": \"Doctor Romantic\",\n" +
                "          \"contenttype\": \"tvshows\",\n" +
                "          \"tcid_2x3_t\": \"1164909683\",\n" +
                "          \"original_row_pos\": 1,\n" +
                "          \"total\": \"20\",\n" +
                "          \"bgcolor\": \"#894040\",\n" +
                "          \"tcid_4x3_t\": \"1164909681\",\n" +
                "          \"paid\": \"false\",\n" +
                "          \"tver\": \"7\",\n" +
                "          \"id\": \"playlist-25721658\",\n" +
                "          \"updatetime\": \"1538475784551\",\n" +
                "          \"tcid_16x9_t\": \"1164909682\",\n" +
                "          \"tcid\": \"1138223865\",\n" +
                "          \"slug\": \"doctor_romantic\",\n" +
                "          \"original_col_pos\": 1\n" +
                "        }\n" +
                "      ],\n" +
                "      \"description\": \"Best Korean SHows\",\n" +
                "      \"logic\": \"Curated\",\n" +
                "      \"id\": \"24_0_30_1\",\n" +
                "      \"title\": \"Best Korean SHows\",\n" +
                "      \"type\": \"list\",\n" +
                "      \"slug\": \"best_korean_shows\",\n" +
                "      \"contenttype\": \"tvshows\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"containertype\": \"list\",\n" +
                "      \"original_row_pos\": 2,\n" +
                "      \"item\": [\n" +
                "        {\n" +
                "          \"videoviews\": \"0\",\n" +
                "          \"subtitle_en_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\",\n" +
                "          \"numshares\": \"0\",\n" +
                "          \"display_title\": \"Don No.1\",\n" +
                "          \"type\": \"clip\",\n" +
                "          \"cue_points\": \"300000,600000,900000,1200000,1500000,1800000,2100000,2400000,2700000,3000000,3300000,3600000,3900000,4200000,4500000,4800000,5100000,5400000,5700000,6000000\",\n" +
                "          \"tur\": \"147:54\",\n" +
                "          \"genrename\": \"Movies\",\n" +
                "          \"allowedregions\": \"WW\",\n" +
                "          \"tdirforwhole\": \"vp63207_V20180529235042\",\n" +
                "          \"id\": \"1160091492\",\n" +
                "          \"href\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/vp63207_V20180529235042/hlsc_e2931.m3u8\",\n" +
                "          \"thumbnailBgColor\": \"#02103A\",\n" +
                "          \"s3url\": \"http://premiumvideo.nc.vuclip.com/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"director\": \"Raghava Lawrence\",\n" +
                "          \"execution_date\": \"09-08-2017\",\n" +
                "          \"iconmaxidx\": \"887\",\n" +
                "          \"download_rights\": \"Yes\",\n" +
                "          \"slugLanguage\": \"Hindi\",\n" +
                "          \"videoviewsimpressions\": \"0\",\n" +
                "          \"tags\": \"Movie, Full Movie, Action, Don No.1\",\n" +
                "          \"subgenre\": \"19\",\n" +
                "          \"release_date\": \"2017-05-18\",\n" +
                "          \"device\": \"Desktop, Mobiles, Tabs, TV\",\n" +
                "          \"original_col_pos\": 2,\n" +
                "          \"end_date\": \"18-04-2019\",\n" +
                "          \"available_subtitle_languages\": \"my,en\",\n" +
                "          \"singer\": \"Shankar Mahadevan\",\n" +
                "          \"content_form\": \"long\",\n" +
                "          \"subgenrename\": \"Full Movie\",\n" +
                "          \"musicdirector\": \"Raghava Lawrence,Chinna\",\n" +
                "          \"simultaneous\": \"-1\",\n" +
                "          \"concurrentstreams\": \"-1\",\n" +
                "          \"subtitle_my_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.srt\",\n" +
                "          \"geo\": \"WW\",\n" +
                "          \"tcid_4x3_t\": \"1160178293\",\n" +
                "          \"genre\": \"9\",\n" +
                "          \"jwmhlsfile\": \"hlsc_me2931.m3u8\",\n" +
                "          \"start_date\": \"09-08-2017\",\n" +
                "          \"number_of_devices\": \"-1\",\n" +
                "          \"product\": \"All,VIU,VIULIFE\",\n" +
                "          \"year_of_release\": \"2007\",\n" +
                "          \"country_origin\": \"IN\",\n" +
                "          \"genderrelevance\": \"Both\",\n" +
                "          \"impressions\": \"0\",\n" +
                "          \"holdback\": \"false\",\n" +
                "          \"actor\": \"Akkineni Nagarjuna\",\n" +
                "          \"allowedregionssearch\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA\",\n" +
                "          \"subsubgenre\": \"147\",\n" +
                "          \"moviealbumshowname\": \"Don No. 1\",\n" +
                "          \"tcid_16x9_t\": \"1160178347\",\n" +
                "          \"complete\": \"true\",\n" +
                "          \"tcid_16x9\": \"1160178249\",\n" +
                "          \"broadcaster\": \"Goldmines Telefilms\",\n" +
                "          \"subtitle_en_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\",\n" +
                "          \"editorrating\": \"2\",\n" +
                "          \"localisedgenrename\": \"Movies\",\n" +
                "          \"index_geo\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA;\",\n" +
                "          \"language\": \"Hindi\",\n" +
                "          \"subsubgenrename\": \"Action\",\n" +
                "          \"themetype\": \"default\",\n" +
                "          \"tver\": \"2\",\n" +
                "          \"adultnonadult\": \"Non Adult\",\n" +
                "          \"vod_type\": \"SVOD,AVOD,FVOD,TVOD\",\n" +
                "          \"slug\": \"don_no_1\",\n" +
                "          \"drm\": \"inhouse\",\n" +
                "          \"actress\": \"Anushka Shetty\",\n" +
                "          \"numberofsearch\": \"0\",\n" +
                "          \"blockedregionssearch\": \"none\",\n" +
                "          \"isgeoblocked\": \"false\",\n" +
                "          \"isAdsAllowed\": \"true\",\n" +
                "          \"deal_type\": \"FF\",\n" +
                "          \"jwhlsfile\": \"hlsc_e2931.m3u8\",\n" +
                "          \"groupid\": \"1160091492\",\n" +
                "          \"original_row_pos\": 0,\n" +
                "          \"download_expiry_duration\": \"0\",\n" +
                "          \"internal_content_rating\": \"C\",\n" +
                "          \"producer\": \"Various\",\n" +
                "          \"prefixforwhole\": \"d38c_\",\n" +
                "          \"hlsfile\": \"hlsc_whe2931.m3u8\",\n" +
                "          \"localisedlanguage\": \"Hindi\",\n" +
                "          \"urlpathd\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"profileforwhole\": \"7\",\n" +
                "          \"mood\": \"Neutral\",\n" +
                "          \"description\": \"A poor man becomes a benevolent gangster but faces deadly threat from an even bigger gangster.\",\n" +
                "          \"watcheddur\": \"0\",\n" +
                "          \"media\": {\n" +
                "            \"subtitles\": {\n" +
                "              \"subtitle\": [\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819695240.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819698021.vtt\"\n" +
                "                }\n" +
                "              ]\n" +
                "            },\n" +
                "            \"videos\": {\n" +
                "              \"video\": {\n" +
                "                \"codingFormat\": {\n" +
                "                  \"files\": {\n" +
                "                    \"file\": [\n" +
                "                      {\n" +
                "                        \"type\": \"CK\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_ck_V20180802002440.mpd\"\n" +
                "                      },\n" +
                "                      {\n" +
                "                        \"type\": \"WV\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_wv_V20180802002440.mpd\"\n" +
                "                      }\n" +
                "                    ]\n" +
                "                  }\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            \"thumbnails\": {\n" +
                "              \"formulaCloudUrl\": \"https://vuclip-a.akamaihd.net/cloudinary/image/fetch/{parameters}/{source_image_url}\",\n" +
                "              \"formulaCdnUrl\": \"https://vuclip-a.akamaihd.net/p/tthumb{w}x{h}/v{v}/d-1/{tcid}.{ext}\"\n" +
                "            }\n" +
                "          },\n" +
                "          \"title\": \"Don No.1\",\n" +
                "          \"platform\": \"MobileWeb,OTT,DesktopWeb,ChromeCast\",\n" +
                "          \"availablesubs\": \"my,en\",\n" +
                "          \"subtitle_my_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.vtt\",\n" +
                "          \"duration\": \"8874\",\n" +
                "          \"deal_region\": \"India\",\n" +
                "          \"contentprovider\": \"Goldmines Telefilms\",\n" +
                "          \"poster_cid\": \"1160178419\",\n" +
                "          \"icondir\": \"80x60\",\n" +
                "          \"mpdckfile\": \"mpegdash_ck/vod_ck_V20180530004335.mpd\",\n" +
                "          \"categoryid\": \"movies\",\n" +
                "          \"cue_points_alg\": \"BASIC\",\n" +
                "          \"numlikes\": \"0\",\n" +
                "          \"adultexplicitvisua\": \"false\",\n" +
                "          \"contenttype\": \"movies\",\n" +
                "          \"aduleexplicitlyric\": \"false\",\n" +
                "          \"args\": \"?c=1160091492&u=5571203139&s=BZjc5Z&abrs\",\n" +
                "          \"cpchannel\": \"South Dubbed Full Movies\",\n" +
                "          \"urlpath\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"paid\": \"true\",\n" +
                "          \"size_vp1\": \"205.2MB\",\n" +
                "          \"size_vp2\": \"277.2MB\",\n" +
                "          \"size_vp3\": \"388.4MB\",\n" +
                "          \"writer\": \"Various\",\n" +
                "          \"tcid\": \"1160091492\",\n" +
                "          \"size_vp4\": \"648.6MB\",\n" +
                "          \"size_vp5\": \"992.9MB\",\n" +
                "          \"size_vp6\": \"1875.3MB\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"description\": \"Action Thriller Movies\",\n" +
                "      \"logic\": \"Curated\",\n" +
                "      \"id\": \"playlist-25542205\",\n" +
                "      \"title\": \"Action Thriller Movies\",\n" +
                "      \"type\": \"list\",\n" +
                "      \"slug\": \"action_thriller_movies\",\n" +
                "      \"contenttype\": \"movies\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"containertype\": \"list\",\n" +
                "      \"original_row_pos\": 2,\n" +
                "      \"item\": [\n" +
                "        {\n" +
                "          \"videoviews\": \"0\",\n" +
                "          \"subtitle_en_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\",\n" +
                "          \"numshares\": \"0\",\n" +
                "          \"display_title\": \"Don No.1\",\n" +
                "          \"type\": \"clip\",\n" +
                "          \"cue_points\": \"300000,600000,900000,1200000,1500000,1800000,2100000,2400000,2700000,3000000,3300000,3600000,3900000,4200000,4500000,4800000,5100000,5400000,5700000,6000000\",\n" +
                "          \"tur\": \"147:54\",\n" +
                "          \"genrename\": \"Movies\",\n" +
                "          \"allowedregions\": \"WW\",\n" +
                "          \"tdirforwhole\": \"vp63207_V20180529235042\",\n" +
                "          \"id\": \"1160091492\",\n" +
                "          \"href\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/vp63207_V20180529235042/hlsc_e2931.m3u8\",\n" +
                "          \"thumbnailBgColor\": \"#02103A\",\n" +
                "          \"s3url\": \"http://premiumvideo.nc.vuclip.com/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"director\": \"Raghava Lawrence\",\n" +
                "          \"execution_date\": \"09-08-2017\",\n" +
                "          \"iconmaxidx\": \"887\",\n" +
                "          \"download_rights\": \"Yes\",\n" +
                "          \"slugLanguage\": \"Hindi\",\n" +
                "          \"videoviewsimpressions\": \"0\",\n" +
                "          \"tags\": \"Movie, Full Movie, Action, Don No.1\",\n" +
                "          \"subgenre\": \"19\",\n" +
                "          \"release_date\": \"2017-05-18\",\n" +
                "          \"device\": \"Desktop, Mobiles, Tabs, TV\",\n" +
                "          \"original_col_pos\": 2,\n" +
                "          \"end_date\": \"18-04-2019\",\n" +
                "          \"available_subtitle_languages\": \"my,en\",\n" +
                "          \"singer\": \"Shankar Mahadevan\",\n" +
                "          \"content_form\": \"long\",\n" +
                "          \"subgenrename\": \"Full Movie\",\n" +
                "          \"musicdirector\": \"Raghava Lawrence,Chinna\",\n" +
                "          \"simultaneous\": \"-1\",\n" +
                "          \"concurrentstreams\": \"-1\",\n" +
                "          \"subtitle_my_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.srt\",\n" +
                "          \"geo\": \"WW\",\n" +
                "          \"tcid_4x3_t\": \"1160178293\",\n" +
                "          \"genre\": \"9\",\n" +
                "          \"jwmhlsfile\": \"hlsc_me2931.m3u8\",\n" +
                "          \"start_date\": \"09-08-2017\",\n" +
                "          \"number_of_devices\": \"-1\",\n" +
                "          \"product\": \"All,VIU,VIULIFE\",\n" +
                "          \"year_of_release\": \"2007\",\n" +
                "          \"country_origin\": \"IN\",\n" +
                "          \"genderrelevance\": \"Both\",\n" +
                "          \"impressions\": \"0\",\n" +
                "          \"holdback\": \"false\",\n" +
                "          \"actor\": \"Akkineni Nagarjuna\",\n" +
                "          \"allowedregionssearch\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA\",\n" +
                "          \"subsubgenre\": \"147\",\n" +
                "          \"moviealbumshowname\": \"Don No. 1\",\n" +
                "          \"tcid_16x9_t\": \"1160178347\",\n" +
                "          \"complete\": \"true\",\n" +
                "          \"tcid_16x9\": \"1160178249\",\n" +
                "          \"broadcaster\": \"Goldmines Telefilms\",\n" +
                "          \"subtitle_en_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\",\n" +
                "          \"editorrating\": \"2\",\n" +
                "          \"localisedgenrename\": \"Movies\",\n" +
                "          \"index_geo\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA;\",\n" +
                "          \"language\": \"Hindi\",\n" +
                "          \"subsubgenrename\": \"Action\",\n" +
                "          \"themetype\": \"default\",\n" +
                "          \"tver\": \"2\",\n" +
                "          \"adultnonadult\": \"Non Adult\",\n" +
                "          \"vod_type\": \"SVOD,AVOD,FVOD,TVOD\",\n" +
                "          \"slug\": \"don_no_1\",\n" +
                "          \"drm\": \"inhouse\",\n" +
                "          \"actress\": \"Anushka Shetty\",\n" +
                "          \"numberofsearch\": \"0\",\n" +
                "          \"blockedregionssearch\": \"none\",\n" +
                "          \"isgeoblocked\": \"false\",\n" +
                "          \"isAdsAllowed\": \"true\",\n" +
                "          \"deal_type\": \"FF\",\n" +
                "          \"jwhlsfile\": \"hlsc_e2931.m3u8\",\n" +
                "          \"groupid\": \"1160091492\",\n" +
                "          \"original_row_pos\": 0,\n" +
                "          \"download_expiry_duration\": \"0\",\n" +
                "          \"internal_content_rating\": \"C\",\n" +
                "          \"producer\": \"Various\",\n" +
                "          \"prefixforwhole\": \"d38c_\",\n" +
                "          \"hlsfile\": \"hlsc_whe2931.m3u8\",\n" +
                "          \"localisedlanguage\": \"Hindi\",\n" +
                "          \"urlpathd\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"profileforwhole\": \"7\",\n" +
                "          \"mood\": \"Neutral\",\n" +
                "          \"description\": \"A poor man becomes a benevolent gangster but faces deadly threat from an even bigger gangster.\",\n" +
                "          \"watcheddur\": \"0\",\n" +
                "          \"media\": {\n" +
                "            \"subtitles\": {\n" +
                "              \"subtitle\": [\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819695240.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819698021.vtt\"\n" +
                "                }\n" +
                "              ]\n" +
                "            },\n" +
                "            \"videos\": {\n" +
                "              \"video\": {\n" +
                "                \"codingFormat\": {\n" +
                "                  \"files\": {\n" +
                "                    \"file\": [\n" +
                "                      {\n" +
                "                        \"type\": \"CK\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_ck_V20180802002440.mpd\"\n" +
                "                      },\n" +
                "                      {\n" +
                "                        \"type\": \"WV\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_wv_V20180802002440.mpd\"\n" +
                "                      }\n" +
                "                    ]\n" +
                "                  }\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            \"thumbnails\": {\n" +
                "              \"formulaCloudUrl\": \"https://vuclip-a.akamaihd.net/cloudinary/image/fetch/{parameters}/{source_image_url}\",\n" +
                "              \"formulaCdnUrl\": \"https://vuclip-a.akamaihd.net/p/tthumb{w}x{h}/v{v}/d-1/{tcid}.{ext}\"\n" +
                "            }\n" +
                "          },\n" +
                "          \"title\": \"Don No.1\",\n" +
                "          \"platform\": \"MobileWeb,OTT,DesktopWeb,ChromeCast\",\n" +
                "          \"availablesubs\": \"my,en\",\n" +
                "          \"subtitle_my_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.vtt\",\n" +
                "          \"duration\": \"8874\",\n" +
                "          \"deal_region\": \"India\",\n" +
                "          \"contentprovider\": \"Goldmines Telefilms\",\n" +
                "          \"poster_cid\": \"1160178419\",\n" +
                "          \"icondir\": \"80x60\",\n" +
                "          \"mpdckfile\": \"mpegdash_ck/vod_ck_V20180530004335.mpd\",\n" +
                "          \"categoryid\": \"movies\",\n" +
                "          \"cue_points_alg\": \"BASIC\",\n" +
                "          \"numlikes\": \"0\",\n" +
                "          \"adultexplicitvisua\": \"false\",\n" +
                "          \"contenttype\": \"movies\",\n" +
                "          \"aduleexplicitlyric\": \"false\",\n" +
                "          \"args\": \"?c=1160091492&u=5571203139&s=BZjc5Z&abrs\",\n" +
                "          \"cpchannel\": \"South Dubbed Full Movies\",\n" +
                "          \"urlpath\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"paid\": \"true\",\n" +
                "          \"size_vp1\": \"205.2MB\",\n" +
                "          \"size_vp2\": \"277.2MB\",\n" +
                "          \"size_vp3\": \"388.4MB\",\n" +
                "          \"writer\": \"Various\",\n" +
                "          \"tcid\": \"1160091492\",\n" +
                "          \"size_vp4\": \"648.6MB\",\n" +
                "          \"size_vp5\": \"992.9MB\",\n" +
                "          \"size_vp6\": \"1875.3MB\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"description\": \"Action Thriller Movies\",\n" +
                "      \"logic\": \"Curated\",\n" +
                "      \"id\": \"playlist-25542205\",\n" +
                "      \"title\": \"Action Thriller Movies\",\n" +
                "      \"type\": \"list\",\n" +
                "      \"slug\": \"action_thriller_movies\",\n" +
                "      \"contenttype\": \"movies\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"containertype\": \"list\",\n" +
                "      \"original_row_pos\": 2,\n" +
                "      \"item\": [\n" +
                "        {\n" +
                "          \"videoviews\": \"0\",\n" +
                "          \"subtitle_en_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\",\n" +
                "          \"numshares\": \"0\",\n" +
                "          \"display_title\": \"Don No.1\",\n" +
                "          \"type\": \"clip\",\n" +
                "          \"cue_points\": \"300000,600000,900000,1200000,1500000,1800000,2100000,2400000,2700000,3000000,3300000,3600000,3900000,4200000,4500000,4800000,5100000,5400000,5700000,6000000\",\n" +
                "          \"tur\": \"147:54\",\n" +
                "          \"genrename\": \"Movies\",\n" +
                "          \"allowedregions\": \"WW\",\n" +
                "          \"tdirforwhole\": \"vp63207_V20180529235042\",\n" +
                "          \"id\": \"1160091492\",\n" +
                "          \"href\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/vp63207_V20180529235042/hlsc_e2931.m3u8\",\n" +
                "          \"thumbnailBgColor\": \"#02103A\",\n" +
                "          \"s3url\": \"http://premiumvideo.nc.vuclip.com/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"director\": \"Raghava Lawrence\",\n" +
                "          \"execution_date\": \"09-08-2017\",\n" +
                "          \"iconmaxidx\": \"887\",\n" +
                "          \"download_rights\": \"Yes\",\n" +
                "          \"slugLanguage\": \"Hindi\",\n" +
                "          \"videoviewsimpressions\": \"0\",\n" +
                "          \"tags\": \"Movie, Full Movie, Action, Don No.1\",\n" +
                "          \"subgenre\": \"19\",\n" +
                "          \"release_date\": \"2017-05-18\",\n" +
                "          \"device\": \"Desktop, Mobiles, Tabs, TV\",\n" +
                "          \"original_col_pos\": 2,\n" +
                "          \"end_date\": \"18-04-2019\",\n" +
                "          \"available_subtitle_languages\": \"my,en\",\n" +
                "          \"singer\": \"Shankar Mahadevan\",\n" +
                "          \"content_form\": \"long\",\n" +
                "          \"subgenrename\": \"Full Movie\",\n" +
                "          \"musicdirector\": \"Raghava Lawrence,Chinna\",\n" +
                "          \"simultaneous\": \"-1\",\n" +
                "          \"concurrentstreams\": \"-1\",\n" +
                "          \"subtitle_my_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.srt\",\n" +
                "          \"geo\": \"WW\",\n" +
                "          \"tcid_4x3_t\": \"1160178293\",\n" +
                "          \"genre\": \"9\",\n" +
                "          \"jwmhlsfile\": \"hlsc_me2931.m3u8\",\n" +
                "          \"start_date\": \"09-08-2017\",\n" +
                "          \"number_of_devices\": \"-1\",\n" +
                "          \"product\": \"All,VIU,VIULIFE\",\n" +
                "          \"year_of_release\": \"2007\",\n" +
                "          \"country_origin\": \"IN\",\n" +
                "          \"genderrelevance\": \"Both\",\n" +
                "          \"impressions\": \"0\",\n" +
                "          \"holdback\": \"false\",\n" +
                "          \"actor\": \"Akkineni Nagarjuna\",\n" +
                "          \"allowedregionssearch\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA\",\n" +
                "          \"subsubgenre\": \"147\",\n" +
                "          \"moviealbumshowname\": \"Don No. 1\",\n" +
                "          \"tcid_16x9_t\": \"1160178347\",\n" +
                "          \"complete\": \"true\",\n" +
                "          \"tcid_16x9\": \"1160178249\",\n" +
                "          \"broadcaster\": \"Goldmines Telefilms\",\n" +
                "          \"subtitle_en_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\",\n" +
                "          \"editorrating\": \"2\",\n" +
                "          \"localisedgenrename\": \"Movies\",\n" +
                "          \"index_geo\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA;\",\n" +
                "          \"language\": \"Hindi\",\n" +
                "          \"subsubgenrename\": \"Action\",\n" +
                "          \"themetype\": \"default\",\n" +
                "          \"tver\": \"2\",\n" +
                "          \"adultnonadult\": \"Non Adult\",\n" +
                "          \"vod_type\": \"SVOD,AVOD,FVOD,TVOD\",\n" +
                "          \"slug\": \"don_no_1\",\n" +
                "          \"drm\": \"inhouse\",\n" +
                "          \"actress\": \"Anushka Shetty\",\n" +
                "          \"numberofsearch\": \"0\",\n" +
                "          \"blockedregionssearch\": \"none\",\n" +
                "          \"isgeoblocked\": \"false\",\n" +
                "          \"isAdsAllowed\": \"true\",\n" +
                "          \"deal_type\": \"FF\",\n" +
                "          \"jwhlsfile\": \"hlsc_e2931.m3u8\",\n" +
                "          \"groupid\": \"1160091492\",\n" +
                "          \"original_row_pos\": 0,\n" +
                "          \"download_expiry_duration\": \"0\",\n" +
                "          \"internal_content_rating\": \"C\",\n" +
                "          \"producer\": \"Various\",\n" +
                "          \"prefixforwhole\": \"d38c_\",\n" +
                "          \"hlsfile\": \"hlsc_whe2931.m3u8\",\n" +
                "          \"localisedlanguage\": \"Hindi\",\n" +
                "          \"urlpathd\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"profileforwhole\": \"7\",\n" +
                "          \"mood\": \"Neutral\",\n" +
                "          \"description\": \"A poor man becomes a benevolent gangster but faces deadly threat from an even bigger gangster.\",\n" +
                "          \"watcheddur\": \"0\",\n" +
                "          \"media\": {\n" +
                "            \"subtitles\": {\n" +
                "              \"subtitle\": [\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819695240.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819698021.vtt\"\n" +
                "                }\n" +
                "              ]\n" +
                "            },\n" +
                "            \"videos\": {\n" +
                "              \"video\": {\n" +
                "                \"codingFormat\": {\n" +
                "                  \"files\": {\n" +
                "                    \"file\": [\n" +
                "                      {\n" +
                "                        \"type\": \"CK\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_ck_V20180802002440.mpd\"\n" +
                "                      },\n" +
                "                      {\n" +
                "                        \"type\": \"WV\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_wv_V20180802002440.mpd\"\n" +
                "                      }\n" +
                "                    ]\n" +
                "                  }\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            \"thumbnails\": {\n" +
                "              \"formulaCloudUrl\": \"https://vuclip-a.akamaihd.net/cloudinary/image/fetch/{parameters}/{source_image_url}\",\n" +
                "              \"formulaCdnUrl\": \"https://vuclip-a.akamaihd.net/p/tthumb{w}x{h}/v{v}/d-1/{tcid}.{ext}\"\n" +
                "            }\n" +
                "          },\n" +
                "          \"title\": \"Don No.1\",\n" +
                "          \"platform\": \"MobileWeb,OTT,DesktopWeb,ChromeCast\",\n" +
                "          \"availablesubs\": \"my,en\",\n" +
                "          \"subtitle_my_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.vtt\",\n" +
                "          \"duration\": \"8874\",\n" +
                "          \"deal_region\": \"India\",\n" +
                "          \"contentprovider\": \"Goldmines Telefilms\",\n" +
                "          \"poster_cid\": \"1160178419\",\n" +
                "          \"icondir\": \"80x60\",\n" +
                "          \"mpdckfile\": \"mpegdash_ck/vod_ck_V20180530004335.mpd\",\n" +
                "          \"categoryid\": \"movies\",\n" +
                "          \"cue_points_alg\": \"BASIC\",\n" +
                "          \"numlikes\": \"0\",\n" +
                "          \"adultexplicitvisua\": \"false\",\n" +
                "          \"contenttype\": \"movies\",\n" +
                "          \"aduleexplicitlyric\": \"false\",\n" +
                "          \"args\": \"?c=1160091492&u=5571203139&s=BZjc5Z&abrs\",\n" +
                "          \"cpchannel\": \"South Dubbed Full Movies\",\n" +
                "          \"urlpath\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"paid\": \"true\",\n" +
                "          \"size_vp1\": \"205.2MB\",\n" +
                "          \"size_vp2\": \"277.2MB\",\n" +
                "          \"size_vp3\": \"388.4MB\",\n" +
                "          \"writer\": \"Various\",\n" +
                "          \"tcid\": \"1160091492\",\n" +
                "          \"size_vp4\": \"648.6MB\",\n" +
                "          \"size_vp5\": \"992.9MB\",\n" +
                "          \"size_vp6\": \"1875.3MB\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"description\": \"Action Thriller Movies\",\n" +
                "      \"logic\": \"Curated\",\n" +
                "      \"id\": \"playlist-25542205\",\n" +
                "      \"title\": \"Action Thriller Movies\",\n" +
                "      \"type\": \"list\",\n" +
                "      \"slug\": \"action_thriller_movies\",\n" +
                "      \"contenttype\": \"movies\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"containertype\": \"list\",\n" +
                "      \"original_row_pos\": 2,\n" +
                "      \"item\": [\n" +
                "        {\n" +
                "          \"videoviews\": \"0\",\n" +
                "          \"subtitle_en_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\",\n" +
                "          \"numshares\": \"0\",\n" +
                "          \"display_title\": \"Don No.1\",\n" +
                "          \"type\": \"clip\",\n" +
                "          \"cue_points\": \"300000,600000,900000,1200000,1500000,1800000,2100000,2400000,2700000,3000000,3300000,3600000,3900000,4200000,4500000,4800000,5100000,5400000,5700000,6000000\",\n" +
                "          \"tur\": \"147:54\",\n" +
                "          \"genrename\": \"Movies\",\n" +
                "          \"allowedregions\": \"WW\",\n" +
                "          \"tdirforwhole\": \"vp63207_V20180529235042\",\n" +
                "          \"id\": \"1160091492\",\n" +
                "          \"href\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/vp63207_V20180529235042/hlsc_e2931.m3u8\",\n" +
                "          \"thumbnailBgColor\": \"#02103A\",\n" +
                "          \"s3url\": \"http://premiumvideo.nc.vuclip.com/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"director\": \"Raghava Lawrence\",\n" +
                "          \"execution_date\": \"09-08-2017\",\n" +
                "          \"iconmaxidx\": \"887\",\n" +
                "          \"download_rights\": \"Yes\",\n" +
                "          \"slugLanguage\": \"Hindi\",\n" +
                "          \"videoviewsimpressions\": \"0\",\n" +
                "          \"tags\": \"Movie, Full Movie, Action, Don No.1\",\n" +
                "          \"subgenre\": \"19\",\n" +
                "          \"release_date\": \"2017-05-18\",\n" +
                "          \"device\": \"Desktop, Mobiles, Tabs, TV\",\n" +
                "          \"original_col_pos\": 2,\n" +
                "          \"end_date\": \"18-04-2019\",\n" +
                "          \"available_subtitle_languages\": \"my,en\",\n" +
                "          \"singer\": \"Shankar Mahadevan\",\n" +
                "          \"content_form\": \"long\",\n" +
                "          \"subgenrename\": \"Full Movie\",\n" +
                "          \"musicdirector\": \"Raghava Lawrence,Chinna\",\n" +
                "          \"simultaneous\": \"-1\",\n" +
                "          \"concurrentstreams\": \"-1\",\n" +
                "          \"subtitle_my_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.srt\",\n" +
                "          \"geo\": \"WW\",\n" +
                "          \"tcid_4x3_t\": \"1160178293\",\n" +
                "          \"genre\": \"9\",\n" +
                "          \"jwmhlsfile\": \"hlsc_me2931.m3u8\",\n" +
                "          \"start_date\": \"09-08-2017\",\n" +
                "          \"number_of_devices\": \"-1\",\n" +
                "          \"product\": \"All,VIU,VIULIFE\",\n" +
                "          \"year_of_release\": \"2007\",\n" +
                "          \"country_origin\": \"IN\",\n" +
                "          \"genderrelevance\": \"Both\",\n" +
                "          \"impressions\": \"0\",\n" +
                "          \"holdback\": \"false\",\n" +
                "          \"actor\": \"Akkineni Nagarjuna\",\n" +
                "          \"allowedregionssearch\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA\",\n" +
                "          \"subsubgenre\": \"147\",\n" +
                "          \"moviealbumshowname\": \"Don No. 1\",\n" +
                "          \"tcid_16x9_t\": \"1160178347\",\n" +
                "          \"complete\": \"true\",\n" +
                "          \"tcid_16x9\": \"1160178249\",\n" +
                "          \"broadcaster\": \"Goldmines Telefilms\",\n" +
                "          \"subtitle_en_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\",\n" +
                "          \"editorrating\": \"2\",\n" +
                "          \"localisedgenrename\": \"Movies\",\n" +
                "          \"index_geo\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA;\",\n" +
                "          \"language\": \"Hindi\",\n" +
                "          \"subsubgenrename\": \"Action\",\n" +
                "          \"themetype\": \"default\",\n" +
                "          \"tver\": \"2\",\n" +
                "          \"adultnonadult\": \"Non Adult\",\n" +
                "          \"vod_type\": \"SVOD,AVOD,FVOD,TVOD\",\n" +
                "          \"slug\": \"don_no_1\",\n" +
                "          \"drm\": \"inhouse\",\n" +
                "          \"actress\": \"Anushka Shetty\",\n" +
                "          \"numberofsearch\": \"0\",\n" +
                "          \"blockedregionssearch\": \"none\",\n" +
                "          \"isgeoblocked\": \"false\",\n" +
                "          \"isAdsAllowed\": \"true\",\n" +
                "          \"deal_type\": \"FF\",\n" +
                "          \"jwhlsfile\": \"hlsc_e2931.m3u8\",\n" +
                "          \"groupid\": \"1160091492\",\n" +
                "          \"original_row_pos\": 0,\n" +
                "          \"download_expiry_duration\": \"0\",\n" +
                "          \"internal_content_rating\": \"C\",\n" +
                "          \"producer\": \"Various\",\n" +
                "          \"prefixforwhole\": \"d38c_\",\n" +
                "          \"hlsfile\": \"hlsc_whe2931.m3u8\",\n" +
                "          \"localisedlanguage\": \"Hindi\",\n" +
                "          \"urlpathd\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"profileforwhole\": \"7\",\n" +
                "          \"mood\": \"Neutral\",\n" +
                "          \"description\": \"A poor man becomes a benevolent gangster but faces deadly threat from an even bigger gangster.\",\n" +
                "          \"watcheddur\": \"0\",\n" +
                "          \"media\": {\n" +
                "            \"subtitles\": {\n" +
                "              \"subtitle\": [\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819695240.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819698021.vtt\"\n" +
                "                }\n" +
                "              ]\n" +
                "            },\n" +
                "            \"videos\": {\n" +
                "              \"video\": {\n" +
                "                \"codingFormat\": {\n" +
                "                  \"files\": {\n" +
                "                    \"file\": [\n" +
                "                      {\n" +
                "                        \"type\": \"CK\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_ck_V20180802002440.mpd\"\n" +
                "                      },\n" +
                "                      {\n" +
                "                        \"type\": \"WV\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_wv_V20180802002440.mpd\"\n" +
                "                      }\n" +
                "                    ]\n" +
                "                  }\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            \"thumbnails\": {\n" +
                "              \"formulaCloudUrl\": \"https://vuclip-a.akamaihd.net/cloudinary/image/fetch/{parameters}/{source_image_url}\",\n" +
                "              \"formulaCdnUrl\": \"https://vuclip-a.akamaihd.net/p/tthumb{w}x{h}/v{v}/d-1/{tcid}.{ext}\"\n" +
                "            }\n" +
                "          },\n" +
                "          \"title\": \"Don No.1\",\n" +
                "          \"platform\": \"MobileWeb,OTT,DesktopWeb,ChromeCast\",\n" +
                "          \"availablesubs\": \"my,en\",\n" +
                "          \"subtitle_my_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.vtt\",\n" +
                "          \"duration\": \"8874\",\n" +
                "          \"deal_region\": \"India\",\n" +
                "          \"contentprovider\": \"Goldmines Telefilms\",\n" +
                "          \"poster_cid\": \"1160178419\",\n" +
                "          \"icondir\": \"80x60\",\n" +
                "          \"mpdckfile\": \"mpegdash_ck/vod_ck_V20180530004335.mpd\",\n" +
                "          \"categoryid\": \"movies\",\n" +
                "          \"cue_points_alg\": \"BASIC\",\n" +
                "          \"numlikes\": \"0\",\n" +
                "          \"adultexplicitvisua\": \"false\",\n" +
                "          \"contenttype\": \"movies\",\n" +
                "          \"aduleexplicitlyric\": \"false\",\n" +
                "          \"args\": \"?c=1160091492&u=5571203139&s=BZjc5Z&abrs\",\n" +
                "          \"cpchannel\": \"South Dubbed Full Movies\",\n" +
                "          \"urlpath\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"paid\": \"true\",\n" +
                "          \"size_vp1\": \"205.2MB\",\n" +
                "          \"size_vp2\": \"277.2MB\",\n" +
                "          \"size_vp3\": \"388.4MB\",\n" +
                "          \"writer\": \"Various\",\n" +
                "          \"tcid\": \"1160091492\",\n" +
                "          \"size_vp4\": \"648.6MB\",\n" +
                "          \"size_vp5\": \"992.9MB\",\n" +
                "          \"size_vp6\": \"1875.3MB\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"description\": \"Action Thriller Movies\",\n" +
                "      \"logic\": \"Curated\",\n" +
                "      \"id\": \"playlist-25542205\",\n" +
                "      \"title\": \"Action Thriller Movies\",\n" +
                "      \"type\": \"list\",\n" +
                "      \"slug\": \"action_thriller_movies\",\n" +
                "      \"contenttype\": \"movies\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"containertype\": \"list\",\n" +
                "      \"original_row_pos\": 2,\n" +
                "      \"item\": [\n" +
                "        {\n" +
                "          \"videoviews\": \"0\",\n" +
                "          \"subtitle_en_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\",\n" +
                "          \"numshares\": \"0\",\n" +
                "          \"display_title\": \"Don No.1\",\n" +
                "          \"type\": \"clip\",\n" +
                "          \"cue_points\": \"300000,600000,900000,1200000,1500000,1800000,2100000,2400000,2700000,3000000,3300000,3600000,3900000,4200000,4500000,4800000,5100000,5400000,5700000,6000000\",\n" +
                "          \"tur\": \"147:54\",\n" +
                "          \"genrename\": \"Movies\",\n" +
                "          \"allowedregions\": \"WW\",\n" +
                "          \"tdirforwhole\": \"vp63207_V20180529235042\",\n" +
                "          \"id\": \"1160091492\",\n" +
                "          \"href\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/vp63207_V20180529235042/hlsc_e2931.m3u8\",\n" +
                "          \"thumbnailBgColor\": \"#02103A\",\n" +
                "          \"s3url\": \"http://premiumvideo.nc.vuclip.com/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"director\": \"Raghava Lawrence\",\n" +
                "          \"execution_date\": \"09-08-2017\",\n" +
                "          \"iconmaxidx\": \"887\",\n" +
                "          \"download_rights\": \"Yes\",\n" +
                "          \"slugLanguage\": \"Hindi\",\n" +
                "          \"videoviewsimpressions\": \"0\",\n" +
                "          \"tags\": \"Movie, Full Movie, Action, Don No.1\",\n" +
                "          \"subgenre\": \"19\",\n" +
                "          \"release_date\": \"2017-05-18\",\n" +
                "          \"device\": \"Desktop, Mobiles, Tabs, TV\",\n" +
                "          \"original_col_pos\": 2,\n" +
                "          \"end_date\": \"18-04-2019\",\n" +
                "          \"available_subtitle_languages\": \"my,en\",\n" +
                "          \"singer\": \"Shankar Mahadevan\",\n" +
                "          \"content_form\": \"long\",\n" +
                "          \"subgenrename\": \"Full Movie\",\n" +
                "          \"musicdirector\": \"Raghava Lawrence,Chinna\",\n" +
                "          \"simultaneous\": \"-1\",\n" +
                "          \"concurrentstreams\": \"-1\",\n" +
                "          \"subtitle_my_srt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.srt\",\n" +
                "          \"geo\": \"WW\",\n" +
                "          \"tcid_4x3_t\": \"1160178293\",\n" +
                "          \"genre\": \"9\",\n" +
                "          \"jwmhlsfile\": \"hlsc_me2931.m3u8\",\n" +
                "          \"start_date\": \"09-08-2017\",\n" +
                "          \"number_of_devices\": \"-1\",\n" +
                "          \"product\": \"All,VIU,VIULIFE\",\n" +
                "          \"year_of_release\": \"2007\",\n" +
                "          \"country_origin\": \"IN\",\n" +
                "          \"genderrelevance\": \"Both\",\n" +
                "          \"impressions\": \"0\",\n" +
                "          \"holdback\": \"false\",\n" +
                "          \"actor\": \"Akkineni Nagarjuna\",\n" +
                "          \"allowedregionssearch\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA\",\n" +
                "          \"subsubgenre\": \"147\",\n" +
                "          \"moviealbumshowname\": \"Don No. 1\",\n" +
                "          \"tcid_16x9_t\": \"1160178347\",\n" +
                "          \"complete\": \"true\",\n" +
                "          \"tcid_16x9\": \"1160178249\",\n" +
                "          \"broadcaster\": \"Goldmines Telefilms\",\n" +
                "          \"subtitle_en_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\",\n" +
                "          \"editorrating\": \"2\",\n" +
                "          \"localisedgenrename\": \"Movies\",\n" +
                "          \"index_geo\": \"MM,IN,BH,KW,OM,QA,SA,AE,ZA,SA;\",\n" +
                "          \"language\": \"Hindi\",\n" +
                "          \"subsubgenrename\": \"Action\",\n" +
                "          \"themetype\": \"default\",\n" +
                "          \"tver\": \"2\",\n" +
                "          \"adultnonadult\": \"Non Adult\",\n" +
                "          \"vod_type\": \"SVOD,AVOD,FVOD,TVOD\",\n" +
                "          \"slug\": \"don_no_1\",\n" +
                "          \"drm\": \"inhouse\",\n" +
                "          \"actress\": \"Anushka Shetty\",\n" +
                "          \"numberofsearch\": \"0\",\n" +
                "          \"blockedregionssearch\": \"none\",\n" +
                "          \"isgeoblocked\": \"false\",\n" +
                "          \"isAdsAllowed\": \"true\",\n" +
                "          \"deal_type\": \"FF\",\n" +
                "          \"jwhlsfile\": \"hlsc_e2931.m3u8\",\n" +
                "          \"groupid\": \"1160091492\",\n" +
                "          \"original_row_pos\": 0,\n" +
                "          \"download_expiry_duration\": \"0\",\n" +
                "          \"internal_content_rating\": \"C\",\n" +
                "          \"producer\": \"Various\",\n" +
                "          \"prefixforwhole\": \"d38c_\",\n" +
                "          \"hlsfile\": \"hlsc_whe2931.m3u8\",\n" +
                "          \"localisedlanguage\": \"Hindi\",\n" +
                "          \"urlpathd\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"profileforwhole\": \"7\",\n" +
                "          \"mood\": \"Neutral\",\n" +
                "          \"description\": \"A poor man becomes a benevolent gangster but faces deadly threat from an even bigger gangster.\",\n" +
                "          \"watcheddur\": \"0\",\n" +
                "          \"media\": {\n" +
                "            \"subtitles\": {\n" +
                "              \"subtitle\": [\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"en\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/en.vtt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"srt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819695240.srt\"\n" +
                "                },\n" +
                "                {\n" +
                "                  \"format\": \"vtt\",\n" +
                "                  \"language\": \"my\",\n" +
                "                  \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/my_1524819698021.vtt\"\n" +
                "                }\n" +
                "              ]\n" +
                "            },\n" +
                "            \"videos\": {\n" +
                "              \"video\": {\n" +
                "                \"codingFormat\": {\n" +
                "                  \"files\": {\n" +
                "                    \"file\": [\n" +
                "                      {\n" +
                "                        \"type\": \"CK\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_ck_V20180802002440.mpd\"\n" +
                "                      },\n" +
                "                      {\n" +
                "                        \"type\": \"WV\",\n" +
                "                        \"url\": \"https://gcpvuclip-a.akamaihd.net/d38c67e76cfa344f5711317bf9c3db01/vp93207_V20180619193016/mpegdash_ck/vod_wv_V20180802002440.mpd\"\n" +
                "                      }\n" +
                "                    ]\n" +
                "                  }\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            \"thumbnails\": {\n" +
                "              \"formulaCloudUrl\": \"https://vuclip-a.akamaihd.net/cloudinary/image/fetch/{parameters}/{source_image_url}\",\n" +
                "              \"formulaCdnUrl\": \"https://vuclip-a.akamaihd.net/p/tthumb{w}x{h}/v{v}/d-1/{tcid}.{ext}\"\n" +
                "            }\n" +
                "          },\n" +
                "          \"title\": \"Don No.1\",\n" +
                "          \"platform\": \"MobileWeb,OTT,DesktopWeb,ChromeCast\",\n" +
                "          \"availablesubs\": \"my,en\",\n" +
                "          \"subtitle_my_vtt\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01/my.vtt\",\n" +
                "          \"duration\": \"8874\",\n" +
                "          \"deal_region\": \"India\",\n" +
                "          \"contentprovider\": \"Goldmines Telefilms\",\n" +
                "          \"poster_cid\": \"1160178419\",\n" +
                "          \"icondir\": \"80x60\",\n" +
                "          \"mpdckfile\": \"mpegdash_ck/vod_ck_V20180530004335.mpd\",\n" +
                "          \"categoryid\": \"movies\",\n" +
                "          \"cue_points_alg\": \"BASIC\",\n" +
                "          \"numlikes\": \"0\",\n" +
                "          \"adultexplicitvisua\": \"false\",\n" +
                "          \"contenttype\": \"movies\",\n" +
                "          \"aduleexplicitlyric\": \"false\",\n" +
                "          \"args\": \"?c=1160091492&u=5571203139&s=BZjc5Z&abrs\",\n" +
                "          \"cpchannel\": \"South Dubbed Full Movies\",\n" +
                "          \"urlpath\": \"https://vuclip-eip2.akamaized.net/d38c67e76cfa344f5711317bf9c3db01\",\n" +
                "          \"paid\": \"true\",\n" +
                "          \"size_vp1\": \"205.2MB\",\n" +
                "          \"size_vp2\": \"277.2MB\",\n" +
                "          \"size_vp3\": \"388.4MB\",\n" +
                "          \"writer\": \"Various\",\n" +
                "          \"tcid\": \"1160091492\",\n" +
                "          \"size_vp4\": \"648.6MB\",\n" +
                "          \"size_vp5\": \"992.9MB\",\n" +
                "          \"size_vp6\": \"1875.3MB\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"description\": \"Action Thriller Movies\",\n" +
                "      \"logic\": \"Curated\",\n" +
                "      \"id\": \"playlist-25542205\",\n" +
                "      \"title\": \"Action Thriller Movies\",\n" +
                "      \"type\": \"list\",\n" +
                "      \"slug\": \"action_thriller_movies\",\n" +
                "      \"contenttype\": \"movies\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"containertype\": \"mixed\",\n" +
                "      \"original_row_pos\": 3,\n" +
                "      \"item\": [\n" +
                "        {\n" +
                "          \"tcid_16x9\": \"1165579048\",\n" +
                "          \"preferred_thumb\": \"withtitle\",\n" +
                "          \"createtime\": \"1577772377347\",\n" +
                "          \"playlistItemsCount\": \"61\",\n" +
                "          \"language\": \"Korean\",\n" +
                "          \"type\": \"playlist\",\n" +
                "          \"title\": \"Left Handed Wife\",\n" +
                "          \"contenttype\": \"tvshows\",\n" +
                "          \"tcid_2x3_t\": \"1165510128\",\n" +
                "          \"original_row_pos\": 0,\n" +
                "          \"total\": \"61\",\n" +
                "          \"bgcolor\": \"#2D374B\",\n" +
                "          \"spotlight_tcid_16x9\": \"1165510127\",\n" +
                "          \"paid\": \"false\",\n" +
                "          \"tver\": \"2\",\n" +
                "          \"id\": \"playlist-25862033\",\n" +
                "          \"updatetime\": \"1577772377347\",\n" +
                "          \"tcid_16x9_t\": \"1165510127\",\n" +
                "          \"tcid_38x13_d\": \"1165575247\",\n" +
                "          \"tcid\": \"1165578978\",\n" +
                "          \"slug\": \"left_handed_wife\",\n" +
                "          \"original_col_pos\": 3\n" +
                "        }\n" +
                "      ],\n" +
                "      \"description\": \"Hot Movies And Shows\",\n" +
                "      \"logic\": \"Curated\",\n" +
                "      \"id\": \"44_0_30_1\",\n" +
                "      \"title\": \"Hot Movies And Shows\",\n" +
                "      \"type\": \"mixed\",\n" +
                "      \"slug\": \"hot_movies_and_shows\",\n" +
                "      \"contenttype\": \"tvshows\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"total\": \"22\",\n" +
                "  \"pageNo\": \"1\",\n" +
                "  \"totalPages\": \"4\",\n" +
                "  \"pageKey\": \"my.app.all\",\n" +
                "  \"pageId\": \"1945d83def7436004a208b3d0b2554e1e-11111v3.my.default\",\n" +
                "  \"fallback\": \"false\"\n" +
                "}";
        try {
            jsonObject = new JSONObject(js);
           addTagDetails(jsonObject ,homePageRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return  jsonObject;
    }

    public void addTagDetails(JSONObject containerObject,HomePageRequest homePageRequest) throws JSONException {
        int fileCounter = 0 ;
            JSONArray itemArray = containerObject.optJSONArray("item");
            for (int m = 0; m < itemArray.length(); m++) {
                JSONObject itemObject = itemArray.optJSONObject(m);
                itemObject.put(Constants.ORIGINAL_COL_POS, m);
                itemObject.put(Constants.ORIGINAL_ROW_POS, fileCounter);
            }
            containerObject.put(Constants.ORIGINAL_ROW_POS, fileCounter);
            fileCounter++;

    }

    @Scheduled(fixedRate = 900000)
    public void evictAllcachesAtIntervals() {
        evictAllCaches();
    }


    public void evictAllCaches() {
        cacheManager.getCacheNames().stream()
                .forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }


}
