package com.hps.hps.Services;

import com.hps.hps.Configs.MasterConfig;
import com.hps.hps.Exception.HomePageException;
import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Services.Factory.ApiServiceFactory;
import com.hps.hps.Utils.HomePageUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class HomePageService {


    @Autowired
    MasterConfig masterConfig;

    @Autowired
    ApiServiceFactory apiServiceFactory;

    @Autowired
    HomePageUtil homePageUtil;

    @Autowired
    URLService urlService ;

    // working loca host : http://localhost:8080/homepage/v1/pages/1?pageId=9b99e1a13d0eedd81300cdc0f1a61c2a.id.default&pageKey=id.app.all&versionName=1.0.92&appid=viu_android&platform=app&format=json&user_new=false&user_geo=10&user_ccode=ID&user_content_preference=all&user_segment=4&app_lang=id

    public JSONObject getCompleteJsonObject(HomePageRequest homePageRequest) throws ExecutionException, InterruptedException, JSONException, HomePageException {
        List<RowAnswer> getSequencefromFile = getSequencefromFile(homePageRequest);
        List<RowAnswer> getYamlConfigDetails = homePageUtil.getRecommendationConfig(masterConfig.getConfig(homePageRequest.getAppid()), homePageRequest);
        Collections.sort(getYamlConfigDetails, getRowComparator());
        List<RowAnswer> mergedList = merge(getSequencefromFile,getYamlConfigDetails);
        List<RowAnswer> paginatedList = getPaginatedSequence(homePageRequest,mergedList);
        return getContentFromList(homePageRequest, paginatedList);
    }

    public JSONObject getDefaultContentPageWise(HomePageRequest homePageRequest) throws ExecutionException, InterruptedException, JSONException, HomePageException {
        List<RowAnswer> getSequencefromFile = getSequencefromFile(homePageRequest);
        return getContentFromList(homePageRequest, getSequencefromFile);
    }

    private JSONObject getContentFromList(HomePageRequest homePageRequest, List<RowAnswer> paginatedList) throws ExecutionException, InterruptedException {
        List<RowAnswer> completeDetailsfromRecommendationApis = getContentFromRecommendationServices(paginatedList , homePageRequest);
        JSONArray js = new JSONArray() ;
        completeDetailsfromRecommendationApis.stream().forEach(ob -> js.put(ob.getServiceResponse()));
        JSONObject jsObject= new JSONObject();
        jsObject.put("container" ,js);
        return jsObject;
    }


    private List<RowAnswer> getPaginatedSequence(HomePageRequest homePageRequest, List<RowAnswer> mergedList) throws HomePageException {
        int start  = ((homePageRequest.getPageNo()-1) * 6) ;
        int end = start + 5 ;
        if(start > mergedList.size()){
            throw new HomePageException("Start Pointer is Greater than Merged Size");
        }else if (end >= mergedList.size()){
            end = mergedList.size();
        }
        return mergedList.subList(start,end);
    }

    private List<RowAnswer> merge(List<RowAnswer> getSequencefromFile, List<RowAnswer> getYamlConfigDetails) {
        List<RowAnswer> ls  = new ArrayList<>();
        for(int i =0 ; i < getSequencefromFile.size(); i++){
            int finalI = i;
            Optional<RowAnswer> rowAnswer= getYamlConfigDetails.stream().filter(o-> o.getRow() == finalI).findAny();
            if(rowAnswer.isPresent()){
                ls.add(rowAnswer.get());
            }else {
                ls.add(getSequencefromFile.get(i));
            }
        }
        return  ls ;
    }

    private Comparator<RowAnswer> getRowComparator() {
        return Comparator.comparing(o -> Integer.valueOf(o.getRow()));
    }

    public  List<RowAnswer> getSequencefromFile(HomePageRequest homePageRequest) throws InterruptedException {
        JSONObject jsonObject = urlService.getContent(homePageRequest);
        List<String> jsonArray = homePageUtil.getJsonPathdataasList("$.container[*].id" ,String.valueOf(jsonObject));
        return  IntStream.range(0,jsonArray.size()).mapToObj(i -> new RowAnswer(i ,   "file" ,String.valueOf(jsonArray.get(i)) ,  null)).collect(Collectors.toList());

    }

    private List<RowAnswer> getContentFromRecommendationServices(List<RowAnswer> getPaginatedList , HomePageRequest homePageRequest) throws ExecutionException, InterruptedException {
        List<CompletableFuture<RowAnswer>> getContentForAllRecommendationServices = getPaginatedList.stream().map(ob -> apiServiceFactory.getService(ob.getServiceName()).getContent(homePageRequest, ob)).collect(Collectors.toList());

        CompletableFuture<Void> allFutures = CompletableFuture
                .allOf(getContentForAllRecommendationServices.toArray(new CompletableFuture[getContentForAllRecommendationServices.size()]));

        CompletableFuture<List<RowAnswer>> allCompletableFuture = allFutures.thenApply(future -> getContentForAllRecommendationServices.stream()
                .map(completableFuture -> completableFuture.join())
                .collect(Collectors.toList()));

        return allCompletableFuture.get();
    }



}
