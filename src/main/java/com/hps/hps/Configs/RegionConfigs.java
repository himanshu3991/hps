package com.hps.hps.Configs;

import lombok.Data;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.Arrays;
import java.util.List;

@Data
public class RegionConfigs {

    private String country ;
    private String usertype ;
    private String usersegment ;
    private String row ;


    public List<String> getUsersegment() {
        return Arrays.asList(usersegment.split("|"));
    }


}
