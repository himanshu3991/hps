package com.hps.hps.Configs;

import lombok.Data;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.Arrays;
import java.util.List;

@Data
public class ApiDetails {
    private String api ;
    private List<RegionConfigs> regionconfigs ;
}
