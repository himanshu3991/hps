package com.hps.hps.Configs.YamlPlatformConfig;

import com.hps.hps.Configs.Config;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "ios")
@Data
public class IOSConfigs extends Config {

}
