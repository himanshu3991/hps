package com.hps.hps.Configs;


import com.hps.hps.Configs.YamlPlatformConfig.AndroidConfigs;
import com.hps.hps.Configs.YamlPlatformConfig.BrowserConfigs;
import com.hps.hps.Configs.YamlPlatformConfig.IOSConfigs;
import com.hps.hps.Configs.YamlPlatformConfig.TVConfigs;
import com.hps.hps.Utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
public class MasterConfig {

    @Autowired
    AndroidConfigs androidConfigs;

    @Autowired
    TVConfigs tvConfigs ;

    @Autowired
    BrowserConfigs browserConfigs ;

    @Autowired
    IOSConfigs iosConfigs ;

    public Config getConfig(String appid){
        switch(appid){
            case Constants.TV: return tvConfigs ;
            case Constants.VIU_ANDROID: return androidConfigs ;
            case Constants.BROWSER: return browserConfigs ;
            case Constants.IOS: return iosConfigs ;
        }
        return null ;
    }
}
