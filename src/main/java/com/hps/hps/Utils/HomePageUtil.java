package com.hps.hps.Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.hps.hps.Configs.Config;
import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RecommendationResponse;
import com.hps.hps.Models.RowAnswer;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HomePageUtil {

    @Value("${pageSize}")
    private int pageSize;

    public static MultiValueMap<String, String> getHeaders(HomePageRequest homePageRequest) {
        MultiValueMap<String,String> headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("x-client-auth","4e27e1d4-4ece-4918-b653-59a9378a9708");
        return headers ;
    }

    public static MultiValueMap<String, String> getHeadersBYW(HomePageRequest homePageRequest) {
        MultiValueMap<String,String> headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization1" , homePageRequest.getHeaders().get(Constants.AUTHORIZATION));
        headers.add("Authorization","Bearer eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2In0.zxZ7QJz249hq_fOetPJQlzA3aj3bzXgpiXGhWmoy3QJX1n152BaWaA.qH7fHsbZdAcibzjVes0bAw.WQWHAt7xE6bzBYyzNiwS0Z997rsrz-DawuFgFj4KI4Ot8dl6fbplYpi4UBtMmxG4Zt6dfEm2t08li25d1Eg4X_bMoMCXTUQHjKGQKs5RA38zKxav-Ea1gaUNZj5eak0xWBi1AB7cgcI4M2_hwy2dF6bnYsBiUMiwInP2JBr81c-QVqrbV_F8MASvDLI95PjHsDVv-uaoS3H6OQB-rpBVFCh4mZhmmCwbbfByu9xjpzAWaqy5-tymfxu1pfaX9_zE3U7cEn8pbS8tOklmIO0z2xkkceNzHuVbbtedu27TfTeKST3E-b0jMTsibANUWBt8.6rNSCYMO7ej_Nqw5p9etuA");
        headers.add("x-client-auth","4e27e1d4-4ece-4918-b653-59a9378a9708");
        headers.add("x-request-id" , "abc");
        headers.add("x-session-id" , "abc");
        return headers ;

    }


    public static String defaultMethod() {
        return null ;
    }


    public  List<RowAnswer> getRecommendationConfig(Config masterConfig, HomePageRequest homePageRequest) {

        return masterConfig.getApis().stream()
                .flatMap(apiDetails ->apiDetails.getRegionconfigs().stream()
                        .filter(regionConfigs -> regionConfigs.getCountry().equals(homePageRequest.getUser_ccode()) && regionConfigs.getUsersegment().contains(homePageRequest.getUser_segment()) )
                        .map(config ->  new RowAnswer( Integer.valueOf(config.getRow()),  apiDetails.getApi(), apiDetails.getApi(), null, null , null ,null, null , null)))
                .collect(Collectors.toList());
    }

    public void tagItems(int i, JSONObject containerObject) throws JSONException {
        JSONArray itemArray = containerObject.optJSONArray("item");
        if(itemArray!=null) {
            for (int m = 0; m < itemArray.length(); m++) {
                JSONObject itemObject = itemArray.optJSONObject(m);
                itemObject.put(Constants.ORIGINAL_COL_POS, i);
                itemObject.put(Constants.ORIGINAL_ROW_POS, m);
            }
        }
    }

    public String getRecentlyWatchedCid(HomePageRequest homepageRequest) {
        String content_id = "" ;
        String recentlyWatched = homepageRequest.getRecently_watched();
        if (StringUtils.trimToNull(recentlyWatched) != null) {
            try {
                JSONArray recentlyWatchedPlaylists = new JSONArray(recentlyWatched);
                if(recentlyWatchedPlaylists.length()>0) {
                    content_id = recentlyWatchedPlaylists.optJSONObject(0).optString(Constants.CONTENT_ID);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return content_id;
    }


        public List<RowAnswer> tag(List<RowAnswer> rowAnswers) {
            rowAnswers.stream().forEach(rowAnswer -> {
                        try {
                            rowAnswer.getServiceResponse().put(Constants.ORIGINAL_ROW_POS , rowAnswer.getRow());
                            tagItems(Integer.valueOf(rowAnswer.getRow()), rowAnswer.getServiceResponse());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
            );
            return rowAnswers;
        }

    public List<String> getJsonPathdataasList(String path , String json  ) {
        DocumentContext context = JsonPath.parse(json);
        return context.read(path);
    }

    public  JSONObject modifyContainer(RowAnswer rowAnswer) throws JsonProcessingException {
        List<String> refinedContents  = getJsonPathdataasList(rowAnswer.getPath(),rowAnswer.getRawContent());
        JSONArray itemsArray = new JSONArray() ;
        for(int i = 0; i <refinedContents.size() ;i++){
            RecommendationResponse obj = new Gson()
                    .fromJson(new JSONArray(String.valueOf(refinedContents.get(i)))
                            .optJSONObject(0).toString(), RecommendationResponse.class);
            obj.setOriginal_col_pos(i);
            obj.setOriginal_row_pos(rowAnswer.getRow());
            itemsArray.put(new JSONObject( obj ));
        }
        JSONObject jsonObject = new JSONObject() ;
        jsonObject.put("containertype", "list");
        jsonObject.put("slug", rowAnswer.getSlug());
        jsonObject.put("title", rowAnswer.getTitle());
        jsonObject.put("recommend", rowAnswer.getRecommend());
        jsonObject.put("item" , itemsArray);
        return  jsonObject ;
    }
}

