package com.hps.hps.Utils;

public class Constants {
    public static final String TV = "tv";
    public static final String ANDROID = "android";

    public static final String JSON = "json";
    public static final String URL = "url";

    public static final String RECOMMEND = "recommend";
    public static final String ORIGINAL_COL_POS = "original_col_pos" ;
    public static final String ORIGINAL_ROW_POS = "original_row_pos";
    public static final String SPLIT = "split";
    public static final String RECOMMENDATION = "recommendation";
    public static final String URLSERVICE = "urlService";
    public static final String TRENDING = "trending";
    public static final String AUTOML = "automl";
    public static final String COLLABORATIVE = "collaborativefilter";
    public static final String BECAUSEYOUWATCHED = "becauseyouwatched";
    public static final String MOMENTS = "moments";
    public static final String BROWSER = "browser" ;
    public static final String IOS = "ios";
    public static final String RELATED = "related" ;
    public static final String CONTENT_ID = "content_id";
    public static final String FILE = "file" ;
    public static final String VIU_ANDROID = "viu_android";
    public static final String AUTHORIZATION = "Authorization" ;

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    public static final String SITE_ID = "siteId";
    public static final String SLOT_ID = "slotId";
    public static final String SID = "sid";
    public static final String IID = "iid";
    public static final String CID = "cid";
    public static final String CLIENTID = "clientId";

    public static final String AUTH_TOKEN = "authToken";
    public static final String AUTH_TOKEN_HEADER = "X-AUTH-TOKEN";
    public static final String LAST_TOKEN = "lastToken";
    public static final String PLAY_TOKEN = "playToken";
    public static final String PLAY_TOKEN_HEADER = "X-PLAY-TOKEN";

    public static final String HTTP_SESS_ID = "httpSessId";
    public static final String HTTP_STATUS = "httpStatus";

    public static final String STATUS = "status";
    public static final String STATUS_SUCCESS = "success";

    public static final String IP = "ip";
    public static final String CCODE = "ccode";

    public static final String URI = "uri";

    public static final String HEADERS = "headers";

    public static final String API = "api";
    public static final String API_HOMEPAGE = "homepage";

    public static final String ACTION = "action";
    public static final String ACTION_REQ = "req";
    public static final String ACTION_RSP = "rsp";
    public static final String xClient = "x-client";
    public static final String xSessionId = "x-session-id";
    public static final String xRequestId = "x-request-id";

    public static final String BATCH_ID = "batchId";
}
