package com.hps.hps.Controllers;


import com.hps.hps.Configs.MasterConfig;
import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Exception.HomePageException;
import com.hps.hps.Services.HomePageService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
public class HomePageController {

    @Autowired
    MasterConfig masterConfig ;

    @Autowired
    HomePageService homePageService ;

    @RequestMapping(value = {"/v1/pages/{pageNo}"},
            method = RequestMethod.GET , produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getHomepage(
            HomePageRequest homePageRequest , @RequestHeader Map<String, String> header , @PathVariable(value = "pageNo") int pageNo) throws ExecutionException, InterruptedException, JSONException, HomePageException {
            homePageRequest.setPageNo(pageNo);
            homePageRequest.setHeaders(header);
            JSONObject result = homePageService.getCompleteJsonObject(homePageRequest);
        return new ResponseEntity<>(String.valueOf(result), HttpStatus.OK);

    }
    // tag the content
    // filter the contnet
    // logging
    // hytrix promtherus
    // output improvement


}
