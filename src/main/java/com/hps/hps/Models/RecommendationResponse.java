package com.hps.hps.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecommendationResponse {
    private int original_row_pos ;
    private int original_col_pos ;
    private String type  = "";
    private String slug ="" ;
    private String id ="" ;
    private String title = "" ;
    private String tcid  = "";
    private String tcid_16x9 ="" ;
    private String tcid_16x6 = "";
    private String tcid_4x3 ="" ;
    private String tcid_2x3 = "";
    private String tcid_4x3_t ="" ;
    private String tcid_2x1 ="" ;
    private String tcid_1x1 ="" ;
    private String tcid_16x6_t = "" ;
    private String tcid_2x3_t = " ";
    private String tcid_1x1_5 = "";
    private String spotlight_tcid_16x9 = "" ;
    private String tcid_16x9_t ="" ;
    private String tcid_38x13_d  = "";
    private String contenttype = "";

}
