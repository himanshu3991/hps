package com.hps.hps.Models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RowAnswer {
    private int row ;
    private String serviceName ;
    private String serviceId ;
    private JSONObject serviceResponse ;
    private String slug ;
    private String title ;
    private String recommend ;
    private String path ;
    private String rawContent ;

    public RowAnswer(int i, String file, String valueOf, Object o) {
        this.row = i ;
        this.serviceName = file ;
        this.serviceId = valueOf ;
        this.serviceResponse = null ;
    }
}
