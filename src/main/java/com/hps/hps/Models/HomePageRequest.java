package com.hps.hps.Models;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpHeaders;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class HomePageRequest {


    private Integer pageNo ;
    private Integer pageSize ;
    private String pageKey ;
    private String pageId ;
    private String url ;
    private String uid ;
    private String iid;
    private Boolean game ;
    private String userId;

    private String recently_watched ;
    private String user_geo ;
    private String user_ccode ;
    private Boolean user_new ;
    private String user_segment  = "-1";
    private String user_content_preference ;
    private String app_lang ;
    private String versionName ;
    private String regionid ;
    private String id ;
    private String format ;
    private String appid ;
    private String ran_sync_no ;
    private String momentCards_start;
    private String momentCards_limit;
    private String momentsCollection_start;
    private String momentsCollection_limit ;
    private String container_id ;
    private  Boolean paginationRequired  ;
    private String platform ;
    private Map<String, String> headers ;


    public String toMomentsStringWithPaginationWithNoRanSync() {
        return "{\"requestType\":\"MOMENTS\",\"addons\":{\"CLIP\":[\"MOMENTS\"]},\"requestParam\":{\"geo\":\""+user_geo+"\",\"contentFlavour\":\""+user_content_preference+"\",\"platform\":\""+appid+"\",\"languageId\":\""+app_lang+"\",\"userId\":\""+uid+"\",\"ccode\":\""+user_ccode+"\",\"id\":\""+container_id+"\",\"type\":\"MOMENTS_COLLECTION\"},\"pagination\":{\"MOMENTS_COLLECTION_PAGE\":{\"start\":"+momentCards_start+",\"limit\":"+momentCards_limit+"},\"MOMENTS_COLLECTION\":{\"start\":"+momentsCollection_start+",\"limit\":"+momentsCollection_limit+",\"type\":\"RANDOM\",\"ran_sync_no\":\""+ran_sync_no
                +"\"}}}";
    }

    public String toMomentsStringWithPagination() {
        return "{\"requestType\":\"MOMENTS\",\"addons\":{\"CLIP\":[\"MOMENTS\"]},\"requestParam\":{\"geo\":\""+user_geo+"\",\"contentFlavour\":\""+user_content_preference+"\",\"platform\":\""+appid+"\",\"languageId\":\""+app_lang+"\",\"userId\":\""+uid+"\",\"ccode\":\""+user_ccode+"\",\"id\":\""+container_id+"\",\"type\":\"MOMENTS_COLLECTION\"},\"pagination\":{\"MOMENTS_COLLECTION_PAGE\":{\"start\":"+momentCards_start+",\"limit\":"+momentCards_limit+"},\"MOMENTS_COLLECTION\":{\"start\":"+momentsCollection_start+",\"limit\":"+momentsCollection_limit+",\"type\":\"RANDOM\",\"ran_sync_no\":\""+ran_sync_no
                +"\"}}}";
    }

    public String toMoments() {
        System.out.println("geetting the moments ");
        return "{\"requestType\":\"MOMENTS\",\"addons\":{\"CLIP\":[\"MOMENTS\"]},\"requestParam\":{\"geo\":\""+user_geo+"\",\"contentFlavour\":\""+user_content_preference+"\",\"platform\":\""+appid+"\",\"languageId\":\""+app_lang+"\",\"userId\":\""+uid+"\",\"ccode\":\""+user_ccode+"\",\"id\":\""+container_id+"\",\"type\":\"MOMENTS_COLLECTION\"},\"pagination\":{\"MOMENTS_COLLECTION_PAGE\":{\"start\":"+momentCards_start+",\"limit\":"+momentCards_limit+"},\"MOMENTS_COLLECTION\":{\"start\":"+momentsCollection_start+",\"limit\":"+momentsCollection_limit+",\"type\":\"RANDOM\",\"ran_sync_no\":\""+ran_sync_no
                +"\"}}}";
    }

    public String toTrending(){
        return "{\"regionid\":\"all\",\"contentFlavour\":\""+user_content_preference+"\",\"languageid\":\""+app_lang+"\",\"ccode\":\""+getUser_ccode()+"\",\"geo\":\""+user_geo+"\",\"fmt\":\"json\",\"platform\":\""+platform+"\"}" ;
    }
}
