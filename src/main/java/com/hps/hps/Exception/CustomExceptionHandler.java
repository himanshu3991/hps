package com.hps.hps.Exception;

import com.hps.hps.Models.ErrorResponse;
import com.hps.hps.Models.HomePageRequest;
import com.hps.hps.Models.RowAnswer;
import com.hps.hps.Services.HomePageService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler
{

    @Autowired
    HomePageService homePageService ;
    @ExceptionHandler(HomePageException.class)
    public final JSONObject handleHomePageException(Exception ex, WebRequest request) throws ExecutionException, InterruptedException {
        int page = 1 ;
        HomePageRequest homePageRequest = new HomePageRequest();
        homePageRequest.setPageNo(page);
        return homePageService.getDefaultContentPageWise(homePageRequest);
    }

    @ExceptionHandler(Exception.class)
    public final JSONObject handleAllExceptions(Exception ex, WebRequest request) throws InterruptedException, ExecutionException {
        int page = 1 ;
        System.out.println("Reaoing an exception");
        HomePageRequest homePageRequest = new HomePageRequest();
        homePageRequest.setPageNo(page);
        return homePageService.getDefaultContentPageWise(homePageRequest);

    }
}