package com.hps.hps.Exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PARTIAL_CONTENT)
public class DefaultValueException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DefaultValueException(String exception) {
        super(exception);
    }
}