package com.hps.hps.Exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class HomePageException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public HomePageException(String exception) {
        super(exception);
    }
}
